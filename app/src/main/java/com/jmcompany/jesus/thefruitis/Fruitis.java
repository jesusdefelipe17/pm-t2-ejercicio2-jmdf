package com.jmcompany.jesus.thefruitis;

public class Fruitis {


    private static final String TABLE_NAME ="fruitis";

    private static final String KEY_NAME ="name";
    private static final String KEY_WEIGHT ="weight";
    private static final String KEY_TYPE ="type";
    private static final String KEY_ROTTEN ="rotten";

    private String name;
    private int weight;
    private String type;
    private int rotten;

    public Fruitis(String name, int weight, String type, int rotten) {
        this.name = name;
        this.weight = weight;
        this.type = type;
        this.rotten = rotten;
    }
    public Fruitis() {

    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static String getKeyName() {
        return KEY_NAME;
    }

    public static String getKeyWeight() {
        return KEY_WEIGHT;
    }

    public static String getKeyType() {
        return KEY_TYPE;
    }

    public static String getKeyRotten() {
        return KEY_ROTTEN;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRotten() {
        return rotten;
    }

    public void setRotten(int rotten) {
        this.rotten = rotten;
    }


    @Override
    public String toString() {
        String rotten_="";
        if(getRotten()==1){
            rotten_="si";
        }else if(getRotten()==0){
            rotten_="no";
        }


        return
                "Name='" + name + '\'' +
                ", Weight=" + weight +
                ", Type='" + type + '\'' +
                ", Rotten=" + rotten_;
    }
}
