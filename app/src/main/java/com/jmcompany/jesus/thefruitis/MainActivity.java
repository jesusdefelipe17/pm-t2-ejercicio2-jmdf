package com.jmcompany.jesus.thefruitis;

import android.content.Intent;
import android.os.Message;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    BaseDatos bbdd=null;
    public static final String EXTRA_MESSAGE = "com.jesus.defelipe.MESSAGE";
    private static final String TAG = "myBBDD";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bbdd = new BaseDatos(this);
        rellenarSpinner();

        EditText get_by_name = findViewById(R.id.editText6);


    }

        public void rellenarSpinner(){

        //Vamos a rellenar los valores

        Spinner spinner_= findViewById(R.id.spinner);

        ArrayList<String> spinner = new ArrayList();
        spinner.add("Fat");
        spinner.add("Acid");
        spinner.add("Sweet");
        spinner.add("Sub-Acid");

        ArrayAdapter spinner_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item,spinner);
        spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_.setAdapter(spinner_adapter);

    }
    public void insertarFruitis(View view) {


        //Recogemos los valores del campo de texto

        //Llamar a la funcion de la base de datos para insertar

        EditText name =  findViewById(R.id.name);
        EditText weight= findViewById(R.id.weight);
        Spinner spinner =  findViewById(R.id.spinner);
        CheckBox checkBox= findViewById(R.id.checkBox);

        String name_ = name.getText().toString();
        int weight_ = Integer.parseInt(weight.getText().toString());
        String type = spinner.getSelectedItem().toString();
        int rotten;

        if(checkBox.isSelected()){//Se supone que si esta seleccionado le pone un 1 y entonces se pone en SI
           rotten =1;
        }else{
            rotten=0;
        }


        Fruitis fruitis = new Fruitis(name_,weight_,type,rotten);

        bbdd.insertarFruitis(fruitis);


    }


    public void mostrarFruitis(View view) {

        ArrayList <String> frutas = new ArrayList<String>();

         frutas = (ArrayList) bbdd.mostrarFrutas();


            Intent intent = new Intent(this, Main2Activity.class);
            intent.putExtra(EXTRA_MESSAGE, frutas);

            startActivity(intent);




    }

    public void mostrarUltimaFruitis(View view) {

        ArrayList <String> frutas = new ArrayList<String>();



        frutas = (ArrayList) bbdd.mostrarUltimaFruta();


        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra(EXTRA_MESSAGE, frutas);

        startActivity(intent);




    }

    public void buscarPorNombre(View view) {

        EditText name =  findViewById(R.id.editText6);
        name.setVisibility(View.VISIBLE);

        String name_=name.getText().toString();


        if(name_.equals("")){

        }else{
            ArrayList <String> frutas = new ArrayList<String>();



            frutas = (ArrayList) bbdd.mostrarFrutaNombre(name_);



            Intent intent = new Intent(this, Main2Activity.class);
            intent.putExtra(EXTRA_MESSAGE, frutas);

            startActivity(intent);
        }


    }
}
