package com.jmcompany.jesus.thefruitis;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {
    private static final String TAG = "myBBDD";
    public static final String EXTRA_MESSAGE = "com.jesus.defelipe.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ArrayList<String> lista = (ArrayList<String>) getIntent().getSerializableExtra(EXTRA_MESSAGE);

       TextView fruitis = findViewById(R.id.textView4);
        String fruititis = "";


        lista.toString();
        for(int i=0;i<lista.size();i++){//Nos recorre el arraylist y nos lo muestra en pantalla
            fruititis += lista.get(i) +"\n\n";
            Log.d(TAG,fruititis);
        }
        fruitis.setText(fruititis);

    }

}
