package com.jmcompany.jesus.thefruitis;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class BaseDatos extends SQLiteOpenHelper {

    private static String DB_NAME = "Fruitis";
    private static int DB_VERSION = 1;
    private static final String TAG = "myBBDD";

    private Context context;
    private SQLiteDatabase db;

    public BaseDatos(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        this.db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        /*
    CREATE TABLE "main"."fruitis" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    "name" TEXT NOT NULL,
    "weight" INTEGER NOT NULL,
    "type" TEXT NOT NULL,
    "rotten" INTEGER
);
     */


        String query ="create table "+ Fruitis.getTableName()+"(_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
                Fruitis.getKeyName()+" TEXT,"+
                Fruitis.getKeyWeight()+" INTEGER,"+
                Fruitis.getKeyType()+" TEXT," +
                Fruitis.getKeyRotten() +" INTEGER " +
                ")";
        sqLiteDatabase.execSQL(query);
        Log.d(TAG,query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }

    public void insertarFruitis(Fruitis fruitis){

        String query;
        this.db = getWritableDatabase();

        if(db!=null){

            query ="INSERT INTO " +Fruitis.getTableName() + " (" + Fruitis.getKeyName() + ", " + Fruitis.getKeyWeight() + ", " + Fruitis.getKeyType() + ", " + Fruitis.getKeyRotten() + ") VALUES ( '" + fruitis.getName() + "', " + fruitis.getWeight() + ", '" + fruitis.getType() + "', " + fruitis.getRotten() + ")";
            Log.d(TAG,query);
            db.execSQL(query);
            db.close();


            //Toast que te diga si se ha insertado correctamente

            Toast.makeText(this.context, "Insertada :)", Toast.LENGTH_LONG).show();
        }else{

            //Error al insertar fruiti
            Toast.makeText(this.context, "Error al insertar", Toast.LENGTH_LONG).show();
        }

    }


public ArrayList<String> mostrarFrutas(){

    ArrayList<String> listaFrutas = new ArrayList<>();

         Fruitis fruitis;


        String query = "select * from " +Fruitis.getTableName();

        this.db = getReadableDatabase();

        Cursor cursor = db.rawQuery(query,null);

        if(cursor.moveToFirst()){

            do{
                fruitis = new Fruitis();


                fruitis.setName(cursor.getString(1));
                fruitis.setWeight(cursor.getInt(2));
                fruitis.setType(cursor.getString(3));
                fruitis.setRotten(cursor.getInt(4));


                listaFrutas.add(fruitis.toString());
                Log.d(TAG,listaFrutas.toString());

            }while(cursor.moveToNext());




        }else{
            //Mensaje al usuario que no se han podido recuperar los datos en forma de toast
            Toast.makeText(this.context, "Error al mostrar las frutas", Toast.LENGTH_LONG).show();
        }

        cursor.close();
        db.close();

        return listaFrutas;


    }

    public ArrayList<String> mostrarUltimaFruta(){

        ArrayList<String> listaFrutas = new ArrayList<>();

        Fruitis fruitis;


        String query = "select * from " +Fruitis.getTableName() + " order by _id desc limit 1";

        this.db = getReadableDatabase();

        Cursor cursor = db.rawQuery(query,null);

        if(cursor.moveToFirst()){

            do{
                fruitis = new Fruitis();


                fruitis.setName(cursor.getString(1));
                fruitis.setWeight(cursor.getInt(2));
                fruitis.setType(cursor.getString(3));
                fruitis.setRotten(cursor.getInt(4));


                listaFrutas.add(fruitis.toString());
                Log.d(TAG,listaFrutas.toString());

            }while(cursor.moveToNext());




        }else{
            //Mensaje al usuario que no se han podido recuperar los datos en forma de toast
            Toast.makeText(this.context, "Error al mostrar las frutas", Toast.LENGTH_LONG).show();
        }

        cursor.close();
        db.close();

        return listaFrutas;


    }

    public ArrayList<String> mostrarFrutaNombre(String name){ //Le pasamos el nombre que recojamos del edittext

        ArrayList<String> listaFrutas = new ArrayList<>();

        Fruitis fruitis;


        String query = "select * from " +Fruitis.getTableName() + " where name ='"+name+"'"; //Buscamos la fruta por su nombre

        this.db = getReadableDatabase();

        Cursor cursor = db.rawQuery(query,null);

        if(cursor.moveToFirst()){

            do{
                fruitis = new Fruitis();


                fruitis.setName(cursor.getString(1));
                fruitis.setWeight(cursor.getInt(2));
                fruitis.setType(cursor.getString(3));
                fruitis.setRotten(cursor.getInt(4));


                listaFrutas.add(fruitis.toString());
                Log.d(TAG,listaFrutas.toString());

            }while(cursor.moveToNext());




        }else{
            //Mensaje al usuario que no se han podido recuperar los datos en forma de toast
            Toast.makeText(this.context, "Error al mostrar las frutas", Toast.LENGTH_LONG).show();
        }

        cursor.close();
        db.close();

        return listaFrutas;


    }




}
